# Drell-Yan Production with Leptoquarks


## Scalar Leptoquarks
The source code for the implementation of the Drell-Yan production with contributions from scalar leptoquarks (SLQs) at $`\mathcal{O}(\alpha_s)`$, published in [ArXiv:2207.00356](https://arxiv.org/abs/2207.00356), can be found in the branch [`Drell-Yan-SLQ-NLO`](https://gitlab.com/lucschnell/Drell-Yan-LQ-NLO/-/tree/Drell-Yan-SLQ-NLO).

### (Older) versions
The different versions of our code can be found at:

| Version  |  Name of the branch | Description |
|---|---|----------- |
|  2.0 |  [`Drell-Yan-SLQ-NLO`](https://gitlab.com/lucschnell/Drell-Yan-LQ-NLO/-/tree/Drell-Yan-SLQ-NLO) |Added the NLO interference contributions to the code, both the virtual and real ones. |
|  1.0 |  [`Drell-Yan-SLQ-NLOv1`](https://gitlab.com/lucschnell/Drell-Yan-LQ-NLO/-/tree/Drell-Yan-SLQ-NLOv1) |First version of our code, as published in [ArXiv:2207.00356](https://arxiv.org/abs/2207.00356).  |


## Vector Leptoquarks (third-generation gauge model)
The source code for the implementation of the Drell-Yan $`pp \to \tau^+ \tau^-`$ production with contributions from third-generation gauge vector leptoquarks (VLQs) at $`\mathcal{O}(\alpha_s)`$, published in [ArXiv:2209.12780](https://arxiv.org/abs/2209.12780), can be found in the branch [`Drell-Yan-VLQ-NLO`](https://gitlab.com/lucschnell/Drell-Yan-LQ-NLO/-/tree/Drell-Yan-VLQ-NLO).

### (Older) versions
The different versions of our code can be found at:


| Version  |  Name of the branch | Description |
|---|---|----------- |
|  2.0 |  [`Drell-Yan-VLQ-NLO`](https://gitlab.com/lucschnell/Drell-Yan-LQ-NLO/-/tree/Drell-Yan-VLQ-NLO) |Added the NLO interference contributions to the code, both the virtual and real ones. Furthermore, the implementation now comes with a version of `LoopTools` included, so it runs out of the box. |
|  1.0 |  [`Drell-Yan-VLQ-NLOv1`](https://gitlab.com/lucschnell/Drell-Yan-LQ-NLO/-/tree/Drell-Yan-VLQ-NLOv1) |First version of our code, as published in [ArXiv:2209.12780](https://arxiv.org/abs/2209.12780). |
